﻿using ByteBank.Funcionarios;
using ByteBank.SistemaInterno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            TesteLogin();
            Console.ReadLine();
        }
        public static void TesteLogin()
        {
            Diretor fun1 = new Diretor("454454646");
            fun1.Senha = "asd";
            SistemaLogin test = new SistemaLogin();
            test.Logar(fun1, "123");

        }
        
        public static void BonificacaoTotal()
        {
            Funcionario fun1 = new Diretor("1");
            Funcionario fun2 = new GerenteDeConta("2");
            Funcionario fun3 = new Designer("3");
            Funcionario fun4 = new Auxiliar("4");
            Funcionario fun5 = new Auxiliar("5");

            GerenciadorBonificacao gerenciador1 = new GerenciadorBonificacao();
            gerenciador1.Registrar(fun1);
            gerenciador1.Registrar(fun2);
            gerenciador1.Registrar(fun3);
            gerenciador1.Registrar(fun4);
            gerenciador1.Registrar(fun5);

            Console.WriteLine("Total investido em bonificação: "+ gerenciador1.GetTotalBonificacao());
        }
    }
}
