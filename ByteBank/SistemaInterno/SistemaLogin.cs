﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.SistemaInterno
{
    public class SistemaLogin
    {
        public bool Logar(IAutenticavel funcionario, string senha)
        {
            
            if (funcionario.Autenticar(senha))
            {
                Console.WriteLine("Acesso Permitido");
                return true;
            }
            else
            {
                Console.WriteLine("Acesso Negado");
                return false;
            }
        }
            
    }
}

