﻿using ByteBank.SistemaInterno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Funcionarios
{
    public class Diretor : FuncionarioAutenticavel, IAutenticavel
    {
        public Diretor(string cpf) : base(cpf, 5000){
            //base realiza a chamada de membros da classe base(classe Funcionario);           
        }

        public override void AumentarSalario()
        {
            Salario *= 1.15;
        }

        public override double GetBonificacao()
        {
            
            return Salario *= 0.5;
        }

    }
}
