﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Funcionarios
{
    public abstract class Funcionario
    {
        public static int TotalDeContas { get; private set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public double Salario { get; protected set; }

        public Funcionario(string cpf, double salario)
        {
            this.Cpf = cpf;
            Salario = salario;
            TotalDeContas++;      
        }

        public abstract void AumentarSalario();

        public abstract double GetBonificacao();

    }
}
