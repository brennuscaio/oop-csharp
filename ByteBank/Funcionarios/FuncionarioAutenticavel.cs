﻿using ByteBank.SistemaInterno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Funcionarios
{
    public abstract class FuncionarioAutenticavel:Funcionario,IAutenticavel
    {
        public string Senha { get; set; }
        public FuncionarioAutenticavel(string cpf, double senha) : base(cpf, senha)
        {

        }
        public bool Autenticar(string senha)
        {
            return Senha == senha;
        }
    }
}
